package main

import (
	"encoding/gob"
	"errors"
	"flag"
	"fmt"
	"hash/fnv"
	"io"
	"log"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"time"
)

var (
	dbdir string

	excludeList listValue = []string{"*~", ".*", "*.dpkg-dist", "*.dpkg-old"}
	includeList listValue

	checksum = flag.Bool("checksum", false, "use checksum of file contents for comparisons, not size/mtime")
)

type listValue []string

func (l *listValue) String() string {
	return strings.Join(*l, ",")
}

func (l *listValue) Set(value string) error {
	if value == "" {
		*l = nil
	} else {
		*l = append(*l, value)
	}
	return nil
}

func init() {
	// The db directory defaults to /var/lib/changed if we are
	// root, otherwise use $HOME/.changed.
	defaultDbdir := "/var/lib/changed"
	if os.Getuid() != 0 {
		defaultDbdir = filepath.Join(os.Getenv("HOME"), ".changed")
	}
	flag.StringVar(&dbdir, "db", defaultDbdir, "state directory")
	flag.Var(&excludeList, "exclude", "exclude files/dirs matching this pattern")
	flag.Var(&includeList, "include", "only include files/dirs matching this pattern")
}

type Walker struct {
	Exclude, Include []string
}

func matchAny(s string, ref []string) bool {
	for _, p := range ref {
		if ok, err := filepath.Match(p, s); err == nil && ok {
			return ok
		}
	}
	return false
}

func (w *Walker) Walk(root string, walkFn filepath.WalkFunc) error {
	return filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			// Only prune directories if they are
			// excluded. But don't skip the root
			// directory, even if it matches an exclude
			// pattern (think ".").
			if path != root && matchAny(filepath.Base(path), w.Exclude) {
				return filepath.SkipDir
			}
			return nil
		}

		name := filepath.Base(path)
		if len(w.Include) > 0 && !matchAny(name, w.Include) {
			return nil
		}
		if len(w.Exclude) > 0 && matchAny(name, w.Exclude) {
			return nil
		}

		walkFn(path, info, err)
		return nil
	})
}

type FileInfo struct {
	Path         string
	Size         int64
	ModTime      time.Time
	Mode         os.FileMode
	Checksum     uint64
	withChecksum bool
}

func (f FileInfo) Encode(w io.Writer) {
	if f.withChecksum {
		fmt.Fprintf(w, "%s,%x\n", f.Path, f.Checksum)
	} else {
		fmt.Fprintf(w, "%s,%d,%d,%d\n", f.Path, f.Size, f.ModTime.Unix(), f.Mode)
	}
}

func newFileInfo(path string, fi os.FileInfo, withChecksum bool) FileInfo {
	var sum uint64
	if withChecksum {
		sum = checksumFile(path)
	}
	return FileInfo{
		Path:         path,
		Size:         fi.Size(),
		ModTime:      fi.ModTime(),
		Mode:         fi.Mode(),
		Checksum:     sum,
		withChecksum: withChecksum,
	}
}

func checksumFile(path string) uint64 {
	file, err := os.Open(path)
	if err != nil {
		log.Printf("%v", err)
		return 0
	}
	defer file.Close()
	h := fnv.New64a()
	io.Copy(h, file)
	return h.Sum64()
}

type FileInfoList []FileInfo

func (l FileInfoList) Len() int           { return len(l) }
func (l FileInfoList) Swap(i, j int)      { l[i], l[j] = l[j], l[i] }
func (l FileInfoList) Less(i, j int) bool { return l[i].Path < l[j].Path }

func (l FileInfoList) Sum() uint64 {
	h := fnv.New64a()
	for _, fi := range l {
		fi.Encode(h)
	}
	return h.Sum64()
}

func fingerprintFiles(rootdir string, exclude, include []string) uint64 {
	var l FileInfoList
	w := &Walker{Include: include, Exclude: exclude}
	w.Walk(rootdir, func(path string, info os.FileInfo, err error) error {
		l = append(l, newFileInfo(path, info, *checksum))
		return nil
	})
	sort.Sort(l)
	return l.Sum()
}

func subtreeDbPath(path string) string {
	h := fnv.New64a()
	io.WriteString(h, path)
	return filepath.Join(dbdir, fmt.Sprintf("%x", h.Sum64()))
}

var errNoFP = errors.New("no previous fingerprint")

// Fetch the saved fingerprint for rootdir, if it exists.
func readPreviousFingerprint(rootdir string) (uint64, error) {
	file, err := os.Open(subtreeDbPath(rootdir))
	if err != nil {
		return 0, errNoFP
	}
	defer file.Close()
	var fp uint64
	n, err := fmt.Fscanf(file, "%x", &fp)
	if err != nil || n == 0 {
		return 0, errNoFP
	}
	return fp, nil
}

// Store the current fingerprint for rootdir in the state directory.
func storeFingerprint(rootdir string, fp uint64) {
	file, err := os.Create(subtreeDbPath(rootdir))
	if err != nil {
		log.Printf("Could not write state file: %v", err)
		return
	}
	defer file.Close()
	fmt.Fprintf(file, "%x", fp)
}

// Read the previous fileset based at rootdir, from the related file
// in the dbdir directory (if it exists).
func readPreviousFileSet(rootdir string) map[string]FileInfo {
	file, err := os.Open(subtreeDbPath(rootdir))
	if err != nil {
		return nil
	}
	defer file.Close()
	var m map[string]FileInfo
	if err := gob.NewDecoder(file).Decode(&m); err != nil {
		return nil
	}
	return m
}

// Store the current fileset based at rootdir to a file in the dbdir
// directory.
func saveFileSet(rootdir string, fset map[string]FileInfo) {
	file, err := os.Create(subtreeDbPath(rootdir))
	if err != nil {
		log.Printf("Could not write state file: %v", err)
		return
	}
	defer file.Close()
	if err := gob.NewEncoder(file).Encode(fset); err != nil {
		log.Printf("Error writing state file: %v", err)
	}
}

// HasChanged returns true if the files below rootdir have changed in
// any way since the last invocation. Files and directories can be
// included or excluded from the scan with simple glob patterns.
func HasChanged(rootdir string, exclude, include []string) bool {
	current := fingerprintFiles(rootdir, exclude, include)
	previous, err := readPreviousFingerprint(rootdir)
	if err == nil && current == previous {
		return false
	}
	storeFingerprint(rootdir, current)
	return true
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	if flag.NArg() < 1 {
		log.Fatal("Usage: changed <PATH> [...]")
	}

	// Ensure dbdir exists.
	if _, err := os.Stat(dbdir); err != nil {
		os.Mkdir(dbdir, 0700)
	}

	for _, path := range flag.Args() {
		abspath, err := filepath.Abs(path)
		if err != nil {
			log.Fatal(err)
		}
		if _, err := os.Stat(abspath); err != nil {
			log.Fatal(err)
		}
		if HasChanged(abspath, excludeList, includeList) {
			os.Exit(0)
		}
	}
	os.Exit(1)
}
